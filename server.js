var fs = require("fs");
var express = require("express");
var bodyParser = require('body-parser');
var multer = require("multer");
var upload = multer();
const xlsx = require('xlsx')
var app = express();
const {formatStrToCompare} = require('./utils')
// for parsing application/json
app.use(bodyParser.json());

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
//form-urlencoded

// for parsing multipart/form-data
//app.use(upload.array());

//static folder
app.use(express.static('public'));


app.get('/', function (req, res) {
    fs.readFile('index.html', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        //console.log(data);
        res.write(data)
    });
});

app.post("/sendFile", upload.single('fileName'), function (req, res) {

    //text fields
    console.log(req.body);

    //file contents
    console.log(req.file);
    let wb = xlsx.read(req.file.buffer, { type: "buffer", cellDates: true, dateNF: 'dd/mm/yyyy;@' });
    const wsname = wb.SheetNames[0];
    const ws = wb.Sheets[wsname];
    let data = xlsx.utils.sheet_to_json(ws, { raw: false });

    // process
    var wbResult = xlsx.utils.book_new();

    /* make worksheet */
    const fields = {
        label: 'Row Labels',
        name: 'Contract owner Name',
        id: 'Số ID',
        dayOfBirth: 'Date of birth Owner',
        houseNumber: 'Permanent address - house number',
        district: 'Permanent address- district',
        province: 'Permanent address- province'
    }
    var ws_data = [
        ["Duplicate Code", fields.label, fields.name, fields.id, fields.dayOfBirth, fields.houseNumber, fields.district, fields.province],
    ];
    let mapDuplicateCode = {}
    data = data.map((r, idx) => {
        r.cName = formatStrToCompare(r[fields.name]);
        r.cDayOfBirth = formatStrToCompare(r[fields.dayOfBirth])
        r.cHouseNumber = formatStrToCompare(r[fields.houseNumber], true)
        r.cDistrict = formatStrToCompare(r[fields.district])
        r.cProvince = formatStrToCompare(r[fields.province])
        // console.log('ROW: ' + (idx + 1))
        return r;
    })

    data.forEach(row => {
        const duplicateCodeField0 = row[fields.id]
        const duplicateCodeField = `${row.cName}${row.cDayOfBirth}`
        const duplicateCodeField1 = `${row.cHouseNumber}${row.cDistrict}${row.cProvince}`
        if (mapDuplicateCode[duplicateCodeField0]) {
            mapDuplicateCode[duplicateCodeField0] += 1;
        } else {
            mapDuplicateCode[duplicateCodeField0] = 1;
            mapDuplicateCode[`${duplicateCodeField0}DuplicateCode`] = (Math.floor(Math.random() * 10000000000000)).toString().slice(-8)
        }

        if (row.cDayOfBirth && row.cDayOfBirth != '(blank)') {
            if (mapDuplicateCode[duplicateCodeField]) {
                mapDuplicateCode[duplicateCodeField] += 1;
            } else {
                mapDuplicateCode[duplicateCodeField] = 1;
                mapDuplicateCode[`${duplicateCodeField}DuplicateCode`] = (Math.floor(Math.random() * 10000000000000)).toString().slice(-8)
            }
        }

        if (mapDuplicateCode[duplicateCodeField1]) {
            mapDuplicateCode[duplicateCodeField1] += 1;
        } else {
            mapDuplicateCode[duplicateCodeField1] = 1;
            mapDuplicateCode[`${duplicateCodeField1}DuplicateCode`] = (Math.floor(Math.random() * 10000000000000)).toString().slice(-8)
        }
    })

    data.forEach((row, idx) => {
        const duplicateCodeField0 = row[fields.id]
        const duplicateCodeField = `${row.cName}${row.cDayOfBirth}`
        const duplicateCodeField1 = `${row.cHouseNumber}${row.cDistrict}${row.cProvince}`
        let duplicateCode = ''

        if (mapDuplicateCode[duplicateCodeField0] > 1) {
            duplicateCode = mapDuplicateCode[`${duplicateCodeField0}DuplicateCode`]
        } else if (mapDuplicateCode[duplicateCodeField] > 1) {
            duplicateCode = mapDuplicateCode[`${duplicateCodeField}DuplicateCode`]
        } else if (mapDuplicateCode[duplicateCodeField1] > 1) {
            duplicateCode = mapDuplicateCode[`${duplicateCodeField1}DuplicateCode`]
        }

        ws_data.push([duplicateCode, row[fields.label], row[fields.name], row[fields.id], row[fields.dayOfBirth], row[fields.houseNumber], row[fields.district], row[fields.province]])
    })

    let wsResult = xlsx.utils.aoa_to_sheet(ws_data);

    console.log('aoa_to_sheet')
    /* Add the worksheet to the workbook */
    xlsx.utils.book_append_sheet(wbResult, wsResult, wsname);
    console.log('book_append_sheet')

    const wbbuf = xlsx.write(wbResult, {
        type: 'buffer' // base64 thì sẽ nhẹ hơn nhưng out of memory
    });
    console.log('xlsx.write')

    res.writeHead(200, {
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'Content-Disposition': 'attachment; filename=mergedResult.xlsx'
    });
    res.end(new Buffer(wbbuf, 'base64'));

    // save as blob
    // var wbbuf = xlsx.write(wbResult, {
    //     type: 'buffer',
    //     bookType: 'xlsx'
    // });
    // res.setHeader('Content-Type', 'application/octet-stream')
    // res.setHeader('Content-Disposition', 'attachment filename=result-duplicated.xlsx')
    // res.send(wbbuf)
});

app.listen(9009, function () {
    console.log('Example app listening on port 9009!')
});