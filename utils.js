const removeAccents = (str) => {
    const AccentsMap = [
        "aàảãáạăằẳẵắặâầẩẫấậ",
        "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
        "dđ", "DĐ",
        "eèẻẽéẹêềểễếệ",
        "EÈẺẼÉẸÊỀỂỄẾỆ",
        "iìỉĩíị",
        "IÌỈĨÍỊ",
        "oòỏõóọôồổỗốộơờởỡớợ",
        "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
        "uùủũúụưừửữứự",
        "UÙỦŨÚỤƯỪỬỮỨỰ",
        "yỳỷỹýỵ",
        "YỲỶỸÝỴ"
    ];
    for (let i=0; i<AccentsMap.length; i++) {
        const re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
        const char = AccentsMap[i][0];
        str = str.replace(re, char);
    }
    return str;
}
const formatStrToCompare = (str, reformatHouseNumber = false) => {
    str = (str || '').split(' ').join('').toLowerCase();
    str = removeAccents(str)
    if (reformatHouseNumber) {
        str = str.replace('p.', 'phuong')
        str = str.split(',').join('')
        str = str.split('.').join('')
        str = str.split('-').join('')
        str = str.split('/').join('')
    }
    return str
}

module.exports.removeAccents = removeAccents
module.exports.formatStrToCompare = formatStrToCompare